/** \file pse_timer1.h
 *  
 */

/** \fn void settimer(int freq, int timer)
 * \brief Timer initialization and configuration with a certain frequency.
 * \param freq Number of increments per second.
 * \param timer Number of the timer to configure.
 */
void settimer(int freq, int timer);

/** \fn void resettimer(int timer)
 * \brief Reset the counter of the desired timer.
 * \param timer Number of the timer to reset.
 */
void resettimer(int timer);

/** \fn int eoctimer(int timer, double time)
 * \brief Verify if the counting has ended.
 * \param timer Number of the timer to verify.
 * \param time Period of counting (seconds).
 * \return Return 1 if ended else return 0. 
 */
int eoctimer(int timer, double time);
